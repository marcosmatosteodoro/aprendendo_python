"""
Em uma associação as classes utilizam umas as outras porem não necessariamente precisam de uma das outras
    para existirem

Uma associaçã é fraca pois se pode excluir um dos elementos e os outros elementos continuam existindo
"""
from classes import Caneta
from classes import MaquinaDeEscrever
from classes import Escritor

escritor = Escritor('Joãozinho')
caneta = Caneta('Bic')
maquina = MaquinaDeEscrever()

#escritor.ferramenta = caneta  # Associando escritor com caneta através de ferramenta que anteriormente = None
escritor.ferramenta = maquina
escritor.ferramenta.escrever()
print(escritor)

del escritor
#print(escritor)
print(caneta.marca)
maquina.escrever()