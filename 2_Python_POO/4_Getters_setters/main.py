""""
Getter obtem um valor
Setter Configura um valor
"""


class Produto:
    def __init__(self, nome, preco):
        self.nome = nome
        self.preco = preco

    def desconto(self, percentual):
        self.preco = self.preco - (self.preco * (percentual / 100))


    @property
    def nome(self):
        return self._nome

    @nome.setter
    def nome(self, valor):
        self._nome = valor.title()

    # Getter
    @property
    def preco(self):
        return self._preco  # Não pode repetir a variável preco pra não dar erro no código

    # Setter
    @preco.setter  # Nome da propriedade + setter
    def preco(self, valor):
        if isinstance(valor, str):  # Se a instância for ...
            valor = float(valor.replace('R$', ''))  # Substituio o R$ por um vazio
        self._preco = valor

    # O preco antes de ser processado na função virá primeiro no getter e será configurado no setter

    # @property
    # def nome(self):
    #     return self._nome
    #
    # @nome.getter
    # def nome(self, valor):
    #     self._nome = valor


p1 = Produto('CAMISETA', 50)
p1.desconto(10)
print(p1.nome, p1.preco)

p2 = Produto('CANECA', 'R$15')
p2.desconto(10)
print(p2.nome, p2.preco)
