class Pessoa:
    ano_atual = 2021

    def __init__(self, nome, idade): # Método de instância
        self.nome = nome
        self.idade = idade

    def get_ano_nascimento(self):
        print(self.ano_atual - self.idade)

    @classmethod  # Método de classe - não precisa de instância para usar
    def por_ano_nascimento(cls, nome, ano_nascimento):  # cls se referencia a class
        idade = cls.ano_atual - ano_nascimento
        return cls(nome, idade)
        # Retorna a própria classe (No caso Pessoa) com os novos parametros

p1 = Pessoa.por_ano_nascimento('Luiz', 1989)
print(p1)
print(p1.nome, p1.idade)
p1.get_ano_nascimento()