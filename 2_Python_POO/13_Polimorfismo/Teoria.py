"""
Polimorfismo é o princípio que permite que as classes derivadas de uma mesma superclasse
    tenham métodos iguais (de mesma assinatura) mas comportamentos diferentes.

Mesma assinatura = Mesma quantidade e tipo de parâmetros

Na aula anterior as classes ContaCorrente e ContaPoupanca se comporta de acordo com o polimorfismo tendo em vista o
    método sacar que ambas possuem porém com comportamentos diferentes
"""
from abc import ABC, abstractmethod

class A(ABC):
    @abstractmethod
    def fala(self, msg): pass

class B(A):
    def fala(self, msg):
        print(f'B está falando {msg}')

class C(A):
    def fala(self, msg):
        print(f'C está falando {msg}')

b = B()
c = C()
b.fala('um assunto.')
c.fala('outro assunto')