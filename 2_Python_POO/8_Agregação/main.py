"""
Uma classe usa outra como parte de si e essa classe precisa da outra classe

ex: um carro e uma roda existem idependentemente da associação, porém uma não funcionam sem a outra
"""

from classes import CarrinhoDeCompras, Produto

carrinho = CarrinhoDeCompras()

p1 = Produto('Camiseta', 50)
p2 = Produto('iPhone', 10_000)
p3 = Produto('Caneca', 15)

carrinho.inserir_produto(p1)
carrinho.inserir_produto(p2)
carrinho.inserir_produto(p3)
carrinho.inserir_produto(p1)
carrinho.inserir_produto(p3)
carrinho.inserir_produto(p3)
carrinho.inserir_produto(p2)


carrinho.lista_produto()
print(carrinho.soma_total())
