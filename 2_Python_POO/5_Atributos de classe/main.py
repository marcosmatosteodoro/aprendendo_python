
class A:
    vc = 123

    def __init__(self):
        self.vc = 321  # DEsta forma todas as instâncias serão alteradas com exeção do valor de classe

a1 = A()
a2 = A()

#A.vc = 321  # Alterando o valor da classe se altera todas as instâncias

a1.vc = 321  # Alterando o valor da instância se altera apenas o valor dela3



print(a1.vc)
print(a2.vc)
print(A.vc)