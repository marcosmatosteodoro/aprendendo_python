"""
Uma classes abstrata pode ter métodos concretos e abstratos e necessãrio importar o módulo abc 

Os métodos concretos são métodos normais dois quais se escreve cógigos e eles funcionam na cadeia de herança

O método abstrato é um metodo   que não tem corpo de tal forma que as classes filhas são obrigadas a criar
    métodos dentro das proprias classes filhas
"""
from abc import ABC, abstractmethod # Classe baseica abstrata

class A(ABC):
    @abstractmethod  # Com este decorador as funções obrigatoriamente tem que ter em todas as classes filhas
    def falar(self):
        pass

class B(A): # Não é possivel instanciar B em A por conta da falta do método falar
    def falar(self):  # Agora pode
        print('Falando...')

a = B()
a.falar()
