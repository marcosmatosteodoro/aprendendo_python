from classes.conta import Conta
from classes.cp import ContaPoupanca
from classes.cc import ContaCorrente

# conta = Conta(1111, 2222, 0)  # Não é possivel instanciar esta classe
cp = ContaPoupanca(1111, 2222, 0)  # agencia, conta, saldo
cp.depositar(10)
cp.sacar(5)

print('#########'*10)

cc = ContaCorrente(1111, 3333, 0, 500)
cc.depositar(100)
cc.sacar(250)
cc.sacar(500)