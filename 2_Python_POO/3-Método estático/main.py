from random import randint

class Pessoa:
    ano_atual = 2021

    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade

    def get_ano_nascimento(self):
        print(self.ano_atual - self.idade)

    @classmethod
    def por_ano_nascimento(cls, nome, ano_nascimento):
        idade = cls.ano_atual - ano_nascimento
        return cls(nome, idade)

    @staticmethod  # Metodo estatico, tipo uma função norma
    def gera_id():
        rand = randint(10000,19999)
        return rand



p1 = Pessoa.por_ano_nascimento('Luiz', 1989)
print(Pessoa.gera_id())
#Não precisa receber o parametro self
print(p1.gera_id())