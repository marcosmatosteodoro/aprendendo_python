"""
Criar um sistema bancário com CLIENTES CONTAS e um BANCO

Cliente tem conta POUPANÇA/CORRENTE e posso SACAR/DEPOSITAR/VIZUALIZAR

Conta corrente tem um LIMITE extra

Pessoa  ->  Cliente

Conta -> ContaCorrente ContaPoupança

Banco
"""


class Pessoa:
    def __init__(self, nome, idade):
        self._nome = nome
        self._idade = idade

    @property
    def nome(self):
        return self._nome

    @property
    def idade(self):
        return self._idade

class Cliente(Pessoa):
    def __init__(self, nome, idade, conta):
        super().__init__(nome, idade)
        self._conta = conta

    @property
    def conta(self):
        return self._conta

class Conta:
    def __init__(self, agencia, conta, saldo):
        self._agencia = agencia
        self._conta = conta
        self._saldo = saldo

    @property
    def agencia(self):
        return self._agencia

    @property
    def conta(self):
        return self._conta

    @property
    def saldo(self):
        return self._saldo

    @saldo.setter
    def saldo(self, valor):
        if not isinstance(valor , (int, float)):
            raise ValueError("Saldo precisa ser numérico")

        self.saldo += valor








