"""
Na programação orientada a objeto clássica se tem modificadores de acesso dos tipos:

public --> métodos e atributos que podem ser utilizados dentro e fora das classes

protected  --> atributos que podem ser acessados apenas dentro da classe

private --> atributo e/ou método só está disponivel dentro da classe
"""


class BaseDeDados:
    def __init__(self):
        self._dados = {}  # Uma chave public

    def inserir_cliente(self, id, nome):
        if 'clientes' not in self._dados:
            self._dados['clientes'] = {id: nome}
        else:
            self._dados['clientes'].update({id: nome})

    def lista_clientes(self):
        for id, nome in self._dados['clientes'].items():
            print(id, nome)

    def apaga_cliente(self, id):
        del self._dados['clientes'][id]


bd = BaseDeDados()
bd.inserir_cliente(1, 'Otávio')
bd.inserir_cliente(2, 'Miranda')
bd.inserir_cliente(3, 'Rose')
bd.inserir_cliente(4, 'Evelin')
bd.apaga_cliente(2)
#bd._dados = 'Uma outra coisa'
""""
Este código quebrou toda a programação para POO clássica bastava utilizar o método 'protected'ou 'private'
    para solucionar, porém no PYTHON não existe esta alternativa
    
No python a classe precedida de '_' não deverá ser acessada novamente ex 'sel._dados = {} ou '__'que desta
    forma não se deve acessar o atributo de maneira nenhuma
"""
bd.lista_clientes()
