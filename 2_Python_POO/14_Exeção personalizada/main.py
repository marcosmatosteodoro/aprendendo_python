"""
Para criar um exeção, a classe tem que terminar com Error e herdar o módulo Exception
"""
class TaErradoError(Exception):
    pass

def testar():
    raise TaErradoError('Errado!')

try:
    testar()
except TaErradoError as error:
    print(f'Erro: {error}')