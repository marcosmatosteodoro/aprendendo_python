"""
Para não repetir o código passamos cliente e aluno com dados herdados de Pessoa
# Este nomeclasse apenas mostra que mesmo com atributos herdados da mesma Classe
ainda são chamados por classes diferentes
"""


class Pessoa:
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade
        self.nomeclasse = self.__class__.__name__ #

    def falando(self):
        print(f'{self.nomeclasse} falando...')


class Cliente(Pessoa):  # Desta forma Cliente herda os atributos de Pessoa
    def comprando(self):
        print(f'{self.nomeclasse} comprando...')

    def falando(self):
        print('Estou em clientes')



class Aluno(Pessoa):  # Desta forma Aluno herda os atributos de Pessoa
    def estudando(self):
        print(f'{self.nomeclasse} estudando...')

class ClienteVIP(Cliente):  # Herança multipla
    def falando(self):  # Sobrepondo o metodo já definido anteriormente
        Pessoa.falando(self)
        super().falando()  # Chamando a função falando na classe superior
        print('Outra coisa qualquer')
