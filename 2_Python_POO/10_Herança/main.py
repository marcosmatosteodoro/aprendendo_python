"""
Associação - Usa | Agregação - Tem | Composição -  É dono | Herança - É
"""

from classes import *

c1 = Cliente('Luiz', 45)
print(c1.nome)
# # c1.falando()
# c1.comprando()
#
# a1 = Aluno('Maria', 54)
# print(a1.nome)
# # a1.falando()
# a1.estudando()
#
# p1 = Pessoa('João', 43)
# p1.falando()

c2 = ClienteVIP('Rose', 25)
c2.falando()