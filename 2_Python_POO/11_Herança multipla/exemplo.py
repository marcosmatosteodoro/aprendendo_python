class A:
    def falar(self):
        print('Falando ... Estou em A.')


class B(A):
    def falar(self):
        print('Falando ... Estou em B')


class C(A):
    def falar(self):
        print('Falando ... Estou em C')


class D(B, C):  # Vai procurar um método da esquerda para a direita
    pass
    # def falar(self):
    #     print('Falando ... Estou em A')

a = A()
b = B()
c = C()
d = D()
a.falar()
b.falar()
c.falar()
d.falar()
