from pessoa import Pessoa1,Pessoa

"""
Um método é uma função dentro de uma classe, para chama-lo utiliza-se: class.método()

Se for criado uma variável sem o préfixo "self" a variável será exclusiva do ecopo da função
"""
p1 = Pessoa1()
p2 = Pessoa1()

p1.nome = 'Luiz'  # p1 != p2 estão instânciados em memórias diferentes, as variáveis são criadas apartir da classe
p2.nome = 'joão'

print(p1)
print(p2)
print(p1.nome)
print(p2.nome)
