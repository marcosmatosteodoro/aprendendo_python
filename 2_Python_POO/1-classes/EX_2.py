from pessoa import Pessoa

p1 = Pessoa('Luiz', 23)
p2 = Pessoa('João', 32)

p1.comer('maçã')
p1.falar('POO')
p1.parar_comer()
p1.falar('POO')
p1.comer('uva')
p1.falar('Python')
p1.parar_falar()
p1.parar_falar()
