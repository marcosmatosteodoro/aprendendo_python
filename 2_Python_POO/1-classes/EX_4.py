from pessoa import Pessoa

p1 = Pessoa('Luiz', 23)
p2 = Pessoa('João', 32)
p3 = Pessoa('Marcos', 24)

print(p1.get_ano_nascimento())
print(p2.get_ano_nascimento())
print(p3.get_ano_nascimento())
print()

print(p1.ano_atual)
print(Pessoa.ano_atual)
