from pessoa import Pessoa

p1 = Pessoa('Luiz', 23)
p2 = Pessoa('João', 32)

p1.falar('POO')
p2.falar('Filmes')
p1.comer('Churrasco')