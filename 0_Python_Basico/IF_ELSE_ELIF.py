idade = eval(input("Informe a idade da criança: "))
if idade < 5:
    print("A criança deve ser vacinada contra gripe.")
    print('Procure o posto de saúde mais próximo.')
elif idade == 5:
    print('A vacina estara disponível em breve.')
    print('Aguarde as próximas informações.')
else:
    print('A vacina só ocorrerá daqui a 3 meses.')
    print('Informe-se novamente neste prazo.')
print('Cuide da saúde sempre. Até mais.')