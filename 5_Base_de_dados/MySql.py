# Instale pip install pymysql
import pymysql.cursors
from contextlib import contextmanager


# Decorador de gerenciador de contexto

@contextmanager
def conecta():
    conexao = pymysql.connect(
        host='127.0.0.1',
        user='root',
        password='',
        db='clientes',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor
    )

    try:
        print('Conexão Iniciada')
        yield conexao
    finally:
        print('Conexão fechada')
        conexao.close()

# Inseri dados na tabela
# with conecta() as conexao:
#     with conexao.cursor() as cursor:
#         sql = 'INSERT INTO clientes (nome, sobrenome, idade, peso) VALUES (%s, %s, %s, %s)'
#         cursor.execute(sql, ('Jack', 'Monrow', 112, 220))
#         conexao.commit()


# with conecta() as conexao:
#     with conexao.cursor() as cursor:
#         sql = 'INSERT INTO clientes (nome, sobrenome, idade, peso) VALUES (%s, %s, %s, %s)'
#
#         dados = [
#             ('Muriel', 'Figueiredo', 19, 55),
#             ('Rose', 'Figueiredo', 19, 55),
#             ('Jose', 'Figueiredo', 19, 55),
#             ('Marcos', 'Paulo', 24, 85),
#         ]
#         # executemany manda vários valores de uma vez
#         cursor.executemany(sql, dados)
#         conexao.commit()


# # Deletar um dado na tabela
# with conecta() as conexao:
#     with conexao.cursor() as cursor:
#         sql = 'DELETE FROM clientes WHERE id = %s'
#         cursor.execute(sql, (6,))
#         conexao.commit()
#
# # Deletar dados na tabela
# with conecta() as conexao:
#     with conexao.cursor() as cursor:
#         sql = 'DELETE FROM clientes WHERE id IN (%s, %s, %s)'
#         cursor.execute(sql, (7, 8, 9))
#         conexao.commit()

# Atualiza dados na tabela
# with conecta() as conexao:
#     with conexao.cursor() as cursor:
#         sql = 'UPDATE clientes SET nome=%s WHERE id=%s'
#         cursor.execute(sql, ('Joana', 5))
#         conexao.commit()

# Ver dados da tabela
with conecta() as conexao:
    with conexao.cursor() as cursor:
        # PARA funcionar termina em clientes
        # DESC decrescente ASC crescente
        cursor.execute('SELECT * FROM clientes ORDER BY id ASC LIMIT 100')
        resultado = cursor.fetchall()

        for linha in resultado:
            print(linha)
