# Se colocar lista = [] por a lista ser mutável o código vai se comportar de maneira estranha
# Colocando o None e o if depois resolvemos o problema
def lista_de_cliente(clientes_iteravel, lista=None):
    if lista is None:
        lista = []
    lista.extend(clientes_iteravel)
    return lista

clientes1 = lista_de_cliente(['João','Maria','Eduardo'])
clientes2 = lista_de_cliente(['Marcos','Jonas','Zico'])
clientes3 = lista_de_cliente(['José'])

print(clientes1)
print(clientes2)
print(clientes3)