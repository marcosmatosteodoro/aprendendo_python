def master(funcao):  # Função master que recebe uma função como parâmetro
    def slave(*args, **kwargs):  # Função slave dentro da função master que executa a função do parâmetro da master
        print('Agora estou decorada')
        funcao(*args, **kwargs)  # Executa a função de parâmetro da função master
    return slave  # Retorna a função slave dentro da master

@master  # Decora função fala_oi com a master
def fala_oi():  # Função que fala OI na tela do usuário
    print('OI')
fala_oi

@master
def outra_funcao(msg):
    print(msg)

outra_funcao('Hello, i am LOKI!')