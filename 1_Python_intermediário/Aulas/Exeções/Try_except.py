"""
TRATANDO EXCEÇÕES
"""
try:
    a = 1/0
except NameError as erro:  # Trata um erro específico
    print('Erro de nome!')
except (IndexError, KeyError) as erro:  # Trata dois erros específics
    print('Erro de index ou de chave!')
except Exception as erro:  # Trata qualquer outr erro
    print('Erro inesperado!')
else:                      # Executado caso não ocorra nenhum erro
    print('Seu código foi executado com sucesso')
# finally:
#     print('Finalmente')     # Independente de ocorrer uma exeção o finally é executado
#     a = ''                   # Um macete para não ter exeções

# print(a)
print('O código não pode parar!!!!!!!!!!!')
