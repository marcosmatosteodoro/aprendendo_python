
# def divide(n1, n2):
#     try:
#         return n1 / n2
#     except ZeroDivisionError as error:
#         print('Log', error)
#         raise # Trata o erro na função, porém deixa o erro passar pra ser tratado posteriormente tambem
#
# try:
#     print(divide(4,0))
# except ZeroDivisionError as error:
#     print(error)

def divide(n1, n2):
    if n2 == 0:
        raise ValueError("n2 não pode ser 0") #Desta forma o erro vai passar com a mensagem que VC escreveu
    return n1 / n2
print(divide(2,0))