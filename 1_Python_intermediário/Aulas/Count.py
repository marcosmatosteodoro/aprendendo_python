"""
count - itertools
    start = 5 comeca a contar de 5
    step = 2 conta de 2 em 2
"""
from itertools import count
contador = count(start=5, step=2)
#print(next(contador))
for i in contador:
    if i > 10:
        break
    print(i)

contador2 = count()
lista = ['Luis','Marcos','Roberto']
lista = zip(contador2,lista)
print(list(lista))  #Criando indìces com contadores
