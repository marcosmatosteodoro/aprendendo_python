import sys

lista = [1,2,3,4,5,6]
print(hasattr(lista,'_iter_'))
l1 = [x for x in range(1000)] #
l2 = (x for x in range(1000)) # Um gerador consome muito menos espaço no armazenamento do computador
print(type(l1), sys.getsizeof(l1))
print(type(l2), sys.getsizeof(l2))
