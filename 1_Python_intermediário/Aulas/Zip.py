"""
Zip - unido iteráveis - Junta as listas utilizando como parâmetro de tamanho a menor lista
zip_longnest -intertools Junta as listas utilizando como parâmetro de tamanho a maior lista substituindo
                por None onde não tem valor, para substituir o none por algo padrão pode se utilizar
                fillvalue='O que quiser'
"""
from itertools import zip_longest #Importando o modo longest do itertools apenas
#import itertools #Importa todo o itertools
### Código
cidades = ['São Paulo','Belo Horizonte','Salvador','Monte Belo']

###Códigos
estado = ['SP','MG','BA']

cidade_estados = zip_longest(cidades,estado, fillvalue='Estado')
# print(next(cidade_estados))
# print(next(cidade_estados))
# print(next(cidade_estados))
for v in cidade_estados:
    print(v)