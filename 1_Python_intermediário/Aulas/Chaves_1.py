d1 = {
    'chave1' : 'valor',
    'chave2' : 'valor2',
    'chave3' : 'valor3',
}
for k in d1: #imprime apenas os nomes das chaves
    print(k)
for v in d1.values(): #imprime apenas os valores
    print(v)
for s in d1.items(): #imprime o par em tuplas
   print(s)
for d in d1.items(): #imprime os valores fora da tupla
    print(d[0],d[1])
for K1,V1 in d1.items():  # OUTRA MANEIRA DE imprimir os valores fora da tupla
    print(K1,V1)