# Módulos padrão do Python
# Módulos são arquivos .py (que contem código Python) e serve para expandir
# as funcionalidades padrão da linguagem
# Veja todos os módulos padrão em
# https://docs.python.org/3/py-modindex.html

# import sys #para importar o módulo por inteito
# print(sys.platform) # Para usar, se colocar MODULO.FUNÇÃO nesse caso sys.platform

from sys import platform as so # importa apenas a funçã platform do módulo system com apelido de so
print(so)
