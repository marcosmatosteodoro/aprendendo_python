# https://docs.python.org/3/library/functions.html#open
with open('abc.txt','w+') as file: # Uma maneira pythonica de abrir um arquivo - Não precisa fechar
    file.write('Linha 1\n')
    file.write('Linha 2\n')
    file.write('Linha 3\n')

    file.seek(0,0)
    print(file.read())

with open('abc.txt', 'a+') as file: # Adiciona mais uma linha no final do arquivo
    file.write('Outra linha ')

with open('abc.txt', 'r') as file:  # Apenas irá ler o arquivo
    print(file.read())