# https://docs.python.org/3/library/functions.html#open
"""
'r'     aberto para leitura (padrão)
'w'     aberto para escrever, truncando o arquivo primeiro
'x'     aberto para criação exclusiva, falhando se o arquivo já existe
'a'     aberto para a escrita, appending até o final do arquivo se ele existe
'b'     modo binário
't'     modo de texto (padrão)
'+'     aberto para atualização (leitura e escrita)
"""

file = open('abc.txt','w+')  # w+ significa que o arquivo "abc.txt" estaráaberto para ler e escrever
file.write('Linha 1\n')
file.write('Linha 2\n')
file.write('Linha 3\n')

file.seek(0,0)
#recoloca o cursor do arquivo novamente para o topo para poder ser lido na função abaixo (tente executar sem esse comando)
print('Lendo linhas:')
print(file.read())  # Lê todo o texto
print('##########################')

file.seek(0,0)
print(file.readline(), end='')  # A função end='' foi usada para retirar a quebra de linnha do 'Linha 1\n'
print(file.readline(), end='')
print(file.readline(), end='')
print('###########################')

file.seek(0,0)
print(file.readlines())  # Ou pode usar o LAÇO FOR para organizar
print('###########################')

file.seek(0,0)
for linha in file.readlines():
    print(linha, end='')
file.close()   # IMPORTANTE NÃO ESQUEÇA DE FECHAR O ARQUIVO CRIADO
