from DADOS import produtos, pessoas, lista
"""
A função MAP() recebe uma função como primeiro argumento e seu resultado é um iterador
"""
#nova_lista = map(lambda x: x*2, lista)
#nova_lista = [x * 2 for x in lista] #Resolvendo problema anterior com listas

# precos = map(lambda p : p['preco'], produtos) #Retornando os preços dos produtos com labda, porém lambda só é
# for preco in precos:                             #Efetivo com expressões
#     print(preco)

def aumenta_preco(p):
    p['preco'] = round(p['preco'] * 1.05, 2) #Define como padrão 2 casas decimais
    return p['preco']

novos_produtos = map(aumenta_preco, produtos)
for produto in novos_produtos:
    print(produto)

def aumenta_idade(p):
    p['nova_idade'] = round(p['idade'] * 1.20)
    return p

idades = map(aumenta_idade, pessoas)
for idade in idades:
    print(idade)