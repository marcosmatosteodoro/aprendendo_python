"""
A função reduce acumula valores, e recebe um FUNÇÃO, ACUMULADOR, UMA LISTA, e um PARAMETRO DE INICIO
"""
from DADOS import produtos, pessoas, lista
from functools import reduce

# soma_lista = reduce(lambda ac, i: i + ac, lista, 0) #O numero 0 é o parämetro inicial
# print(soma_lista)

# soma_precos = reduce(lambda ac, p:p['preco'] + ac, produtos, 0)
# print(soma_precos)

soma_idade = reduce(lambda ac, i: i['idade'] + ac, pessoas, 0)
print(soma_idade/len(pessoas))