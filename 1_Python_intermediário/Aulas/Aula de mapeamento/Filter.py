"""
A função filter recebe uma função e uma expressão boleana para tratar
"""
from DADOS import produtos, pessoas, lista

# nova_lista = filter(lambda x: x > 5, lista) #Numeros da lista maior que 5
# print(list(nova_lista))

nova_lista2 = filter(lambda p : p['preco'] > 10 ,produtos) #produtos maiores que 10 no preço
for produto in nova_lista2:
    print(produto)