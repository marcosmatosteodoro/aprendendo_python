"""
Combinations - Combinção a ordem não importa e não repetem valores únicos
Permutation - Permutação a ordem importa e não repetem valores únicos
Product - Produto a ordem importa e repete valores únicos
"""
from itertools import combinations,permutations,product
pessoas = ['Luiz','Andre','Eduardo','Leticia','Fabricio','Rose',]
# for grupo in combinations(pessoas , 2): # Luis,Andre
#     print(grupo)
# for grupo in permutations(pessoas , 2): #Luis,Andre  Andre,Luis
#     print(grupo)
for grupo in product(pessoas , repeat=2): #Luis,Luis  Luis,Andre  Andre,Luis
    print(grupo)