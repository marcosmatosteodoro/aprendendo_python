s1 = {1,2,3,4,5}
s1.add('Marcos') #adiciona termo
s1.update('pyton') #adiciona termo por índice
s1.discard(4) #Remove termo
s1.clear() #Apaga dados do set
print(s1,type(s1))
s2 = {1,2,3,4,5,7}
s3 = {1,2,3,4,5,6}
su = s2 | (s3) #A união dos sets
si = s2 & s3 #A interceção dos sets
sd = s3 - s2 #A diferença dos sets
sq = s2 ^ s3 #Uni os elementos que não são comuns
print(sq)