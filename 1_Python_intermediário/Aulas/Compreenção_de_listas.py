l1 = [1,2,3,4,5,6,7,8,9]
l2 = ['Luiz','Mauro', 'Maria']
l3 = list(range(100))
tupla = (
    ('Chave1','Valor1'),
    ('Chave2','Valor2')
)
ex1 = [v for v in l1]
ex2 = [v*2 for v in l1]
ex3 = [(v,v2) for v in l1 for v2 in range(3)]
ex4 = [v.replace('a','@').upper() for v in l2]
ex5 = [(y,x) for x,y in tupla]
ex6 = [v for v in l3 if v % 3 == 0 if v % 8 == 0]
ex7 = [v if v % 3 == 0 else 'Não é!' for v in l3]

print(ex6)