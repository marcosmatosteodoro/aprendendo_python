carrinho = []

carrinho.append(('Produto 1', 30))
carrinho.append(('Produto 2', 20))
carrinho.append(('Produto 3', 50))

preco = sum([float(p) for v,p in carrinho])
print(f"O valor total da compra foi R${preco:.2f}")