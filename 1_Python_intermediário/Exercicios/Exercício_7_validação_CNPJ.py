"""
04.252.011/0001-10  40.688.134/0001-61  71.506.168/0001-11  12.544.992/0001-05

04252011000110       40688134000161       71506168000111     12544992000105
"""
indice, total_d1, total_d2 = 0,0,0
cnpj_ent = "04252011000110"
cnpj_novo = cnpj_ent[:-2]

for index in range(5,-7,-1):
    if index < 2:
        index = index + 8
    c = index + 1
    if index == 9:
        c = 2
    total_d1 += + int(cnpj_novo[indice]) * index
    total_d2 += + int(cnpj_novo[indice]) * c
    indice +=1
    if indice == 12:
        digito1 = 11 - (total_d1 % 11)
        if digito1 > 9:
            digito1 = 0

        total_d2 = total_d2 + digito1 * 2
        digito2 = 11 - (total_d2 % 11)
        if digito2 > 9:
            digito2 = 0

CNPJ = (f'{cnpj_ent[0:2]}.{cnpj_ent[2:5]}.{cnpj_ent[5:8]}/{cnpj_ent[8:12]}-{cnpj_ent[12:14]}')
cnpj_v = cnpj_novo + str(digito1) + str(digito2)

if cnpj_ent == cnpj_v:
    print(f'CNPJ: {CNPJ} é VÁLIDO!!')
else:
    print(f'CNPJ: {CNPJ} INVÁLIDO')


