import re






def apenas_numeros(cnpj):
    v = re.sub(r'[^0-9]','',cnpj)
    return v,v[:-2]

def alg_valida(cnpj):
    indice, total_d1, total_d2 = 0, 0, 0
    cnpj = cnpj[1]
    for index in range(5, -7, -1):
        if index < 2:
            index = index + 8
        N_index = index + 1
        if index == 9:
            N_index = 2
        total_d1 += + int(cnpj[indice]) * index
        total_d2 += + int(cnpj[indice]) * N_index
        indice += 1

    return total_d1,total_d2

def digito(total):
    digito1 = 11 - (total[0] % 11)
    if digito1 > 9:
        digito1 = 0
    digito2 = 11 - ((total[1] + 2 * digito1) % 11)
    if digito2 > 9:
        digito2 = 0
    return digito1,digito2

def Valida(cnpj,digitos):
    cnpjV = cnpj[1] + str(digitos[0]) + str(digitos[1])
    if cnpjV == cnpj[0]:
        return (f'CNPJ {cnpj[0]} é VÁLIDO')

    else:
        return (f'CNPJ {cnpj[0]} é INVALIDO')

def faz_tudo(cnpj): # 04.252.011/0001-1
     CP = apenas_numeros(cnpj)
     return Valida(CP,digito(alg_valida(CP)))






# if indice == 12:
#     digito1 = 11 - (total_d1 % 11)
#     if digito1 > 9:
#         digito1 = 0
#
#     total_d2 = total_d2 + digito1 * 2
#     digito2 = 11 - (total_d2 % 11)
#     if digito2 > 9:
#         digito2 = 0