print()
print('Texto explicativo')
print()
perguntas = {
    'Pergunta 1':{
        'pergunta':'Quanto é 2+2?',
        'respostas': {'a':'1','b':'4','c':'5',},
        'resposta_certa':'b',
    },
    'Pergunta 2': {
        'pergunta': 'Quanto é 3*2?',
        'respostas': {'a': '5', 'b': '1', 'c': '6', },
        'resposta_certa': 'c',

    }
}
respostas_certas_u = 0
print()
for chave_pergunta, chave_resposta in perguntas.items():
    print(f'{chave_pergunta}:{chave_resposta["pergunta"]}')
    print('Respostas: ')
    for rk, rv in chave_resposta['respostas'].items():
        print(f'[{rk}]: {rv}')

    resposta_usuario = input('Sua resposta: ')

    if resposta_usuario == chave_resposta['resposta_certa']:
        print('Você acertou!')
        respostas_certas_u += 1
    else:
        print('Você errou!!!!!!!')
    print()
qt_perguntas = len(perguntas)
porcent_acerto = respostas_certas_u / qt_perguntas * 100
if porcent_acerto >= 60:
    Frase_final = "PARABENS!"
else:
    Frase_final = "Estude mais."
print(f'Você acertou {respostas_certas_u} perguntas com {porcent_acerto}% de aproveitamento. {Frase_final}')
